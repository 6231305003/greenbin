import Vue from 'vue'
import * as VeeValidate from 'vee-validate'
import App from './App.vue'
import router from './router'


import vuetify from './plugins/vuetify'

Vue.config.productionTip = false
Vue.use(VeeValidate)

new Vue({
  vuetify,
  router,
  render: h => h(App)
}).$mount('#app')
