import Vue from 'vue'
import Router from 'vue-router'


import index from './views/index.vue'
import test from './views/test.vue'
import reward from './views/Rewards.vue'
import menu from './views/Menu.vue'
import recycle from './views/recycle.vue'
import complete from './views/Success.vue'

Vue.use(Router)

const router = new Router({
    mode: 'history',
    base: process.env.BASE_URL,
    routes: [
      {
        path: '/',
        name: 'index',
        component: index,
      },
      {
        path: '/test',
        name: 'test',
        component: test,
      },
      {
        path: '/reward',
        name: 'Rewards',
        component: reward,
      },
      {
        path: '/menu',
        name: 'Menu',
        component: menu,
      },
      {
        path: '/recycle',
        name: 'Recycle',
        component: recycle,
      },
      {
        path: '/complete',
        name: 'Complete',
        component: complete,
      },
      // {
      //   path: '/PDQResults/:points',
      //   name: 'PDQResults',
      //   component: Result,
      // },
      
    ]
})

export default router