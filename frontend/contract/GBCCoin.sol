// SPDX-License-Identifier: MIT

pragma solidity ^0.8.0;

contract GreenBinCoin{
    uint _totalSupply;
    mapping(address => uint) _balances;
    
    function name() public pure returns (string memory){
        return "GreenBin Coin";
    }
    function symbol() public pure returns (string memory){
        return "GBC";
    }
    function decimals() public pure returns (uint8) {
        return 0;
        
    }
    function totalSupply() public view returns (uint256) {
        return _totalSupply;
        
    }
    function balanceOf(address _owner) public view returns (uint256 balance){
        return _balances[_owner];
    }
    function transfer(address _to, uint256 _value) public returns (bool success){
        _balances[msg.sender] -= _value;
        _balances[_to]+= _value;
        return true;
    }
    
    function transferFrom(address _from, address _to, uint256 _value) public returns (bool success){
        _balances[_from] -= _value;
        _balances[_to] += _value;
        return true;
    }
    
    function mint(address _to , uint _value) public {
        _balances[_to] += _value;
        _totalSupply += _value;
    }
}