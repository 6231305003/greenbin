import web3 from './web3';

const abi = [
	{
		"inputs": [],
		"stateMutability": "nonpayable",
		"type": "constructor"
	},
	{
		"anonymous": false,
		"inputs": [
			{
				"indexed": false,
				"internalType": "address",
				"name": "voter",
				"type": "address"
			},
			{
				"indexed": true,
				"internalType": "uint256",
				"name": "number",
				"type": "uint256"
			}
		],
		"name": "Tranfers",
		"type": "event"
	},
	{
		"inputs": [],
		"name": "_owner",
		"outputs": [
			{
				"internalType": "address",
				"name": "",
				"type": "address"
			}
		],
		"stateMutability": "view",
		"type": "function"
	},
	{
		"inputs": [
			{
				"internalType": "uint256",
				"name": "number",
				"type": "uint256"
			}
		],
		"name": "tranfers",
		"outputs": [],
		"stateMutability": "nonpayable",
		"type": "function"
	}
]; // THE ABI
// Here is just only abi because we haven't created auction yet.
export default address => {
  const instance = new web3.eth.Contract(abi, address);
  return instance;
};
