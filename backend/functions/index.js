const functions = require("firebase-functions");
const admin = require('firebase-admin');

const express = require('express');
const bodyParser = require('body-parser');

admin.initializeApp(functions.config().firestore);

const database = admin.firestore();
const collectionName = "users";

const app = express();
const main = express();

main.use('/api/v1',app);
main.use(bodyParser.json());
main.use(bodyParser.urlencoded({extended : true} ) );

let users_obj = {
    phone : "",
    address : 0,

}

exports.getAllUsers = functions.https.onRequest( async(req , res) =>{
    try{
        const users_query_snapshot = await database.collection(collectionName).get();
        const arrayResults = []

        users_query_snapshot.forEach(
            (doc) =>{
                arrayResults.push({
                    id : doc.id,
                    data : doc.data()   
                })
            }
        )
        res.status(200).json(arrayResults);

    }catch(err){
        res.status(500).send(error);
    }
});

// Get Specific Student services
exports.getSpecificUsers = functions.https.onRequest( async ( req , res ) => {
    try {
        const Id = req.query.phone
        database.collection(collectionName).doc(Id).get()
        .then( users => { 
            if(!users.exists) throw new Error('Users not found');
            res.status(200).json({ id : users.id , data : users.data() })
        })
    } catch (error) {
        res.status(500).send(error)
    }
});      

// Add New Student service
exports.addUsers = functions.https.onRequest( async ( req , res ) => {
    try {
        const users = {
            phone : req.body['phone'],
            points : req.body['points'],
        }

        const new_doc = await database.collection(collectionName).add(users);
        res.status(201).send(`Create a new user : ${new_doc}`)
    } catch (error) {
        res.status(400).send('User should contain all things!')
    }
});

// Update Specific Student service
exports.updateSpecificUsers = functions.https.onRequest( async ( req , res ) => {
    const Id = req.query.id;
    await database.collection(collectionName).doc(Id).set(req.body,{ merge :true })
    .then( () => res.json({ id : Id }) )
    .catch( (error) => res.status(500).send(error) )
});

// Delete Specific Student service
exports.deleteSpecificUsers = functions.https.onRequest( async ( req , res ) => {
    const Id = req.query.id
    database.collection(collectionName).doc(Id).delete()
    .then( () => res.status(204).send("Document successfully deleted!"))
    .catch( function (error) {
        res.status(500).send(error);
    })
});

// // Create and Deploy Your First Cloud Functions
// // https://firebase.google.com/docs/functions/write-firebase-functions
//
// exports.helloWorld = functions.https.onRequest((request, response) => {
//   functions.logger.info("Hello logs!", {structuredData: true});
//   response.send("Hello from Firebase!");
// });
